#! /usr/bin/python
"""
It is used readability format for bss properties
If needed customised output add
example
"SSID" : self._ssid
and remove prop.udate that key
and add your function definition
example
def _ssid(self):
   ....
"""
from .signature_convert import Signature
from .common import convert


class Util(object):
    """
    It is bss properties output format is changed
    """
    def __init__(self, key, val):
        """
        key: bss property
        val: bss, dbus output
        """
        self.key = key
        self.val = val
        self.value = None
        self.property = None
        prop = {
            "SSID": self._ssid,
            "BSSID": self._bssid,
        }
        prop.update(dict.fromkeys(['WPA', 'RSN', 'WPS', 'IEs', 'Privacy', 'Mode',
                                   'Frequency', 'Rates', 'Signal', 'Age'], self._common_convert))
        prop[self.key]()

    def get_property(self):
        """
        customised dbus properties output
        """
        return self.property

    def _ssid(self):
        self.value = ''.join(Signature(self.val).convert_ay_str())
        self.property = '{} = {}'.format(self.key, self.value)

    def _bssid(self):
        temp_bssid = ''.join([":%02x" % byte for byte in self.val])
        self.value = temp_bssid[1:]
        self.property = '{} = {}'.format(self.key, self.value)

    def _common_convert(self):
        self.value = convert(self.val)
        self.property = '{} = {}'.format(self.key, self.value)

    def get_key(self):
        """
        Get bss properties value like "SSID","BSSID",... etc
        """
        return self.key

    def get_value(self):
        """
        Get dbus value
        """
        return self.value
