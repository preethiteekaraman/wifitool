#!/usr/bin/env python
# coding=utf-8

# Copyright (c) Mikhail Mamrouski.
# See LICENSE for details.

"""
Module provides helper functions.
"""

import sys
from collections import namedtuple
from functools import partial

import dbus

PY3 = (sys.version_info[0] == 3)


def convert(dbus_obj):
    """Converts dbus_obj from dbus type to python type.

    :param dbus_obj: dbus object.
    :returns: dbus_obj in python type.
    """
    _isinstance = partial(isinstance, dbus_obj)
    convert_type = namedtuple('convert_type', 'pytype dbustypes')

    pyint = convert_type(int, (dbus.Byte, dbus.Int16, dbus.Int32, dbus.Int64,
                               dbus.UInt16, dbus.UInt32, dbus.UInt64))
    pybool = convert_type(bool, (dbus.Boolean, ))
    pyfloat = convert_type(float, (dbus.Double, ))
    pylist = convert_type(lambda _obj: list(map(convert, dbus_obj)),
                          (dbus.Array, ))
    pytuple = convert_type(lambda _obj: tuple(map(convert, dbus_obj)),
                           (dbus.Struct, ))
    types_str = (dbus.ObjectPath, dbus.Signature, dbus.String)

    if not PY3:
        types_str += (dbus.UTF8String,)
    pystr = convert_type(str if PY3 else unicode, types_str)

    pydict = convert_type(
        lambda _obj: dict(zip(map(convert, dbus_obj.keys()),
                              map(convert, dbus_obj.values()))),
        (dbus.Dictionary, )
    )

    for conv in (pyint, pybool, pyfloat, pylist, pytuple, pystr, pydict):
        if any(map(_isinstance, conv.dbustypes)):
            return conv.pytype(dbus_obj)
    else:
        return dbus_obj
