"""
Scanned BSSs information's
"""
from .iface import Iface


class Bss(Iface):

    '''
    Interface implemented by objects representing a scanned BSSs,
    i.e., scan results.
    So call interface get properties BSSs
    BSS interface declare on scan_done function .scan signal
    call scan_don function only so declares bss interface in
    scan_done function
    '''

    def __init__(self, ifname):
        super(Bss, self).__init__(ifname)
        super(Bss, self).making_method_calls()
        super(Bss, self).scan()

    def scan_done(self, success):
        '''
        Get the no of bss object path
        '''
        super(Bss, self).scan_done(success)
        self.wpas_dbus_interface = "fi.w1.wpa_supplicant1.BSS"

    def get_interfaces(self):
        print self.get_prop("interfaces")
        
if __name__ == "__main__":
    bss = ss("wlan0")
    bss.get_interfaces
    
