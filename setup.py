"""
Packages wpa supplicant
"""
from setuptools import setup, find_packages

setup(
    name="wifitool",
    version="0.0.1",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'wifitool = wpa_supp.__main__:main'
        ]
    },
)
