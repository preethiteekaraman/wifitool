# Wi-Fi Analysis Tool

Wi-Fi Analysis Tool is a handy tool to access wpa_supplicant methods,
properties and signal strengths. This project is helpful in automating
the Wi-Fi based testcases and developing the Wi-Fi testing framework.

## Getting Started

For installing the tool, follow the instruction below.

## Prerequisites

Python Version >= 2.7

## Installation methods

### Method 1:

````bash
~$ python setup.py install --user
````

### Method 2:

````bash
~$ python setup.py sdist
~$ cd dist
~$ pip install --user wpa_supplicant-0.0.1.tar.gz
````

## Executing the command

````bash
~$ wifitool bss wlan0
````

## Uninstaled

````bash
~$ pip uninstall wifitool -y
````
